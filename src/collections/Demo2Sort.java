package collections;

import java.util.*;

public class Demo2Sort {
  public static void main(String[] args) {
    List<String> names = new ArrayList<>();
    names.add("Lenterra");
    names.add("Manami");
    names.add("Leonid");
    names.add("William");
    names.add("Georgio");
    System.out.println(names);
    Collections.sort(names);
    System.out.println("Alphabetically :"+names);
    // sorting cars
    List<Car> homogeneousParkingLot = new ArrayList<>();
    homogeneousParkingLot.add(new Car("Volkswagen", 70));
    homogeneousParkingLot.add(new Car("Citroen", 2));
    homogeneousParkingLot.add(new Car("Audi", 140));
    Collections.sort(homogeneousParkingLot);
    System.out.println("Sorted cars  :"+homogeneousParkingLot);


  }
}
