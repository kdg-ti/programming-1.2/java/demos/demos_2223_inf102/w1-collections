package collections;

public class Plane {
  String brand;
  int maxLoad;

  public Plane(String brand, int maxLoad) {
    this.brand = brand;
    this.maxLoad = maxLoad;
  }

  @Override
  public String toString() {
    return "Plane{" +
        "brand='" + brand + '\'' +
        ", maxLoad=" + maxLoad +
        '}';
  }
}
