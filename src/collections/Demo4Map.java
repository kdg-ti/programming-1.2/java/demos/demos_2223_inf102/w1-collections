package collections;

import java.util.*;

public class Demo4Map {
  public static void main(String[] args) {
    List<String> names = new ArrayList<>();
    names.add("Lenterra");
    names.add("Manami");
    names.add("Leonid");
    names.add("William");
    names.add("Lenterra");
    names.add("Georgio");
    System.out.println("Names list:" +names);



    // hashset
    Map<String,Car> homogeneousParkingLot = new HashMap<>();
    Car citroen = new Car("Citroen", 2);
    homogeneousParkingLot.put(citroen.getBrand(),citroen);
    homogeneousParkingLot.put("Audi",new Car("Audi", 140));
    homogeneousParkingLot.put("VolksWagen",new Car("Volkswagen", 70));
    System.out.println("Cars hashmap  :"+homogeneousParkingLot);
    System.out.println( homogeneousParkingLot.get("Citroen"));



  }
}
