package collections;

import java.util.*;

public class Demo1Collections {
  public static void main(String[] args) {
    Car[] cars = new Car[10];
    // adding one by one
    cars[0] = new Car("Tesla", 500);
    cars[1] = new Car("BMW", 150);
    Car[] allAtOnceCars = {new Car("Tesla", 500),
        new Car("BMW", 150)};
    // I can only put cars in a Car[]
    // cars[2] = new Plane("Boeing",5000);

    for (Car car : cars) {
      System.out.print(car + ", ");
    }
    System.out.println();

    // an array of int
    int[]numbers = new int[4];
    numbers[0]=1;


    List parkingLot = new ArrayList();
    parkingLot.add(new Car("Volkswagen", 70));
    parkingLot.add(new Car("Citroen", 2));
    parkingLot.add(new Plane("Airbus", 10_000));

    // I can create a list with all objects at once, but it is IMMUTABLE
    List parkingAllAtOnce = List.of(new Car("Volkswagen", 70),
        new Car("Citroen", 2));
    System.out.println(parkingLot);

    // This does not work on an immutable collection
    // parkingAllAtOnce.add(new Car ("Redbull",1000));
    // How to make an immutable collection mutable?
    // Create a new list from it
    List mutableAllAtOnce = new ArrayList<>(parkingAllAtOnce);
    mutableAllAtOnce.add(new Car ("Redbull",1000));
    System.out.println("Mutable Collection:" + mutableAllAtOnce);

  // How do we make collections which can only contain one type
    // using generics
   // List<Car> homogeneousParkingLot = new ArrayList<Car>();
    List<Car> homogeneousParkingLot = new ArrayList<>();
    homogeneousParkingLot.add(new Car("Volkswagen", 70));
    homogeneousParkingLot.add(new Car("Citroen", 2));
    // homogeneous collection does only accept cars
    // homogeneousParkingLot.add(new Plane("Airbus", 10_000));

    // A List does not support primitives only Objets
    // List<int> listNumbers= new ArrayList<int> ();
    List<Integer> listNumbers= new ArrayList<> ();
    listNumbers.add( Integer.valueOf(5));
    // autoboxing
    listNumbers.add( 6);
    // autoUnboxing
    int firstNumber = listNumbers.get(0);

    // looping over a collection
    System.out.println("Classic for loop over a collection");

    for(int i = 0; i< homogeneousParkingLot.size();i++){
      System.out.print(homogeneousParkingLot.get(i) + ", ");
    }
    System.out.println();

    System.out.println("Enhanced for loop over a collection");

    for(Car car : homogeneousParkingLot){
      System.out.print(car + ", ");
    }
    System.out.println();

    // switching positions in the array
    System.out.println("Switching elements at positions 0 and 1");
    Car firstCar = homogeneousParkingLot.get(0);
    homogeneousParkingLot.set(0,homogeneousParkingLot.get(1));
    homogeneousParkingLot.set(1,firstCar);
    System.out.println(homogeneousParkingLot);

    System.out.println("Loop over a collection using an iterator");

    for(Iterator<Car> iter8r = homogeneousParkingLot.iterator() ; iter8r.hasNext();){
      System.out.print(iter8r.next() + ", ");
    }
    System.out.println();

    System.out.println("Removing during a loop is UNSAFE");
//    for(int i = 0; i< homogeneousParkingLot.size();i++){
//      if(homogeneousParkingLot.get(i).getBrand().equals("Citroen")){
//        homogeneousParkingLot.remove(i);
//      }
//    }
//    System.out.println(homogeneousParkingLot);
    System.out.println("Removing during a loop , only with iterator");

    for(Iterator<Car> iter8r = homogeneousParkingLot.iterator() ; iter8r.hasNext();){
      if(iter8r.next().getBrand().equals("Citroen") ){
        iter8r.remove();
      }
    }
    System.out.println(homogeneousParkingLot);
  }



}
