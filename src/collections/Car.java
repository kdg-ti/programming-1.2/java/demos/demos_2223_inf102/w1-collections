package collections;

import java.util.Objects;

public class Car implements Comparable
{
  private String brand;
  private int horsePower;

  public Car(String brand, int horsePower) {
    this.brand = brand;
    this.horsePower = horsePower;
  }

  @Override
  public String toString() {
    return "Car{" +
        "brand='" + brand + '\'' +
        ", horsePower=" + horsePower +
        '}';
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public int getHorsePower() {
    return horsePower;
  }

  public void setHorsePower(int horsePower) {
    this.horsePower = horsePower;
  }

  //@Override
  public int compareTo(Object o) {
    // compare by brand
   // return getBrand().compareTo(((Car)o).getBrand());
    // compare by brand, reversed
   // return getBrand().compareTo(((Car)o).getBrand());
    // compare by horsepower using Integer
    return ((Integer)getHorsePower()).compareTo(((Car)o).getHorsePower());
   // compare  by horsepower using int
    //  return getHorsePower() -((Car)o).getHorsePower()  ;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Car car)) {
      return false;
    }
    return brand.equals(car.brand);
  }

  @Override
  public int hashCode() {
    return Objects.hash(brand);
  }
}
