package collections;

import java.util.*;

public class Demo3Set {
  public static void main(String[] args) {
    List<String> names = new ArrayList<>();
    names.add("Lenterra");
    names.add("Manami");
    names.add("Leonid");
    names.add("William");
    names.add("Lenterra");
    names.add("Georgio");
    System.out.println("Names list:" +names);

    Set<String> nameSet = new HashSet<>();
    nameSet.add("Lenterra");
    nameSet.add("Manami");
    nameSet.add("Leonid");
    nameSet.add("William");
    nameSet.add("Lenterra");
    nameSet.add("Georgio");
    System.out.println("Name set: "+nameSet);

    // hashset
    Set<Car> homogeneousParkingLot = new HashSet<>();
    homogeneousParkingLot.add(new Car("Citroen", 2));
    homogeneousParkingLot.add(new Car("Audi", 140));
    homogeneousParkingLot.add(new Car("Audi", 140));
    homogeneousParkingLot.add(new Car("Volkswagen", 70));
    System.out.println("Cars hashset  :"+homogeneousParkingLot);

    // treeset
    Set<Car> parkingLotSet = new TreeSet<>();
    parkingLotSet.add(new Car("Citroen", 2));
    parkingLotSet.add(new Car("Audi", 140));
    parkingLotSet.add(new Car("Audi", 140));
    parkingLotSet.add(new Car("Volkswagen", 70));

    System.out.println("Cars treeset  :"+parkingLotSet);
    parkingLotSet.add(new Car("Prosche", 100));

    System.out.println("Cars treeset  :"+parkingLotSet);

  }
}
